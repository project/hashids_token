<?php
/**
 * @file hashids_token.tokens.inc
 */
use Drupal\Core\Render\BubbleableMetadata;


/**
 * Implements hook_token_info().
 */
function hashids_token_token_info() {

  $types['hashid'] = [
    'name' => t("Hashid"),
    'description' => t("Tokens related to ids generated from numbers by Hashids lib."),
  ];

  $hashid['node-nid'] = [
    'name' => t("Content ID"),
    'description' => t("The hashid from the unique ID of the content item, or node."),
  ];

  $hashid['node-vid'] = [
    'name' => t("Revision ID"),
    'description' => t("The hashid from the unique ID of the node's latest revision."),
  ];

  $hashid['current-user-uid'] = [
    'name' => t("User ID"),
    'description' => t("The hashid from the unique ID of the user account."),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'hashid' => $hashid,
    ],
  ];
}


/**
 * Implements hook_tokens().
 */
function hashids_token_tokens($type, $tokens, array $data, array $options,
                                               BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'hashid') {

    $hashids = \Drupal::service('hashids');

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'node-nid':
            if (!empty($data['node'])) {
                $replacements[$original] = $hashids->generate($data['node']->get('nid')->value);
            }
          break;
        case 'node-vid':
            if (!empty($data['node'])) {
                $replacements[$original] = $hashids->generate($data['node']->get('vid')->value);
            }
          break;
        case 'current-user-uid':
            if (!empty($data['user'])) {
                $replacements[$original] = $hashids->generate($data['user']->get('uid')->value);
            }
            break;
      }
    }
  }

  return $replacements;
}