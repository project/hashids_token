
## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## INTRODUCTION

Hashids Token module provides useful hashids tokens.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/hashids_token

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/hashids_token

## REQUIREMENTS

* Token (https://www.drupal.org/project/token):
  Provides additional tokens not supported by core.

* Hashids (https://www.drupal.org/project/hashids):
  Hashids is a small library that generates short, unique, non-sequential ids from numbers.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

- No configuration is needed.

## MAINTAINERS

Current maintainers:
 * Wanderson Reis (wasare) - https://www.drupal.org/u/wasare
